interface TypeCheck {
    balance: number,
    dateIssue: string,
    endTerm: string,
    currency: string,
    active: boolean,
}

interface CreditLimit {
    creditLimit: number,
}

interface Check {
    debit: TypeCheck,
    credit: TypeCheck & CreditLimit,
}

interface Clients {
    name: string,
    surname: string,
    activeClients: boolean,
    registrationDate: string,
    check: Check,
}

let bankClients: Clients[] = [{
    name: 'Alex',
    surname: 'Alexis',
    activeClients: false,
    registrationDate: '12-09-2020',
    check: {
        debit: {
            balance: 20000,
            dateIssue: '12-09-2020',
            endTerm: '12-09-2025',
            currency: 'USD',
            active: true,
        },
        credit: {
            balance: 5000,
            creditLimit: 10000,
            dateIssue: '12-09-2021',
            endTerm: '12-09-2026',
            currency: 'RUR',
            active: false,
        },
    }
},
{
    name: 'Aleksandr',
    surname: 'Hoggarth',
    activeClients: true,
    registrationDate: '20-01-2015',
    check: {
        debit: {
            balance: 0,
            dateIssue: '25-01-2015',
            endTerm: '25-01-2020',
            currency: 'RUR',
            active: false,
        },
        credit: {
            balance: 5000,
            creditLimit: 10000,
            dateIssue: '25-05-2015',
            endTerm: '25-05-2020',
            currency: 'USD',
            active: true,
        },
    }
},
{
    name: 'Kirill',
    surname: 'Nash',
    activeClients: false,
    registrationDate: '20-01-2010',
    check: {
        debit: {
            balance: 0,
            dateIssue: '25-01-2011',
            endTerm: '25-01-2016',
            currency: 'EUR',
            active: false,
        },
        credit: {
            balance: 12000,
            creditLimit: 20000,
            dateIssue: '25-05-2011',
            endTerm: '25-05-2016',
            currency: 'USD',
            active: true,
        },
    }
},
{
    name: 'Aleksey',
    surname: 'Holiday',
    activeClients: true,
    registrationDate: '20-01-2021',
    check: {
        debit: {
            balance: 50000,
            dateIssue: '25-01-2021',
            endTerm: '25-01-2026',
            currency: 'UAH',
            active: true,
        },
        credit: {
            balance: 24000,
            creditLimit: 30000,
            dateIssue: '25-05-2021',
            endTerm: '25-05-2026',
            currency: 'EUR',
            active: true,
        },
    }
},
{
    name: 'Anatoly',
    surname: 'James',
    activeClients: true,
    registrationDate: '20-01-2019',
    check: {
        debit: {
            balance: 10000,
            dateIssue: '25-01-2019',
            endTerm: '25-01-2024',
            currency: 'UAH',
            active: true,
        },
        credit: {
            balance: 5000,
            creditLimit: 5000,
            dateIssue: '25-05-2019',
            endTerm: '25-05-2024',
            currency: 'EUR',
            active: true,
        },
    }
},
]

class Bank {
    bankClients: Clients[];

    constructor(param: Clients[]) {
        this.bankClients = param;
    }

    serviceRequest(param: Record<string, number> , callback: Function) {
        let url = `https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5`;
        return fetch(url).then(data => data.json()).then(mass => {
            let currency: Record<string, string> = {};
            for(let index in mass){
                currency[mass[index].ccy] = mass[index].buy;
            }
            return callback(param, currency);
        })
    }

    totalAmountMoney() {
        let param: Record<string, number> = {};

        this.bankClients.forEach(item => {
            param[item.check.debit.currency] = param[item.check.debit.currency] || 0;
            param[item.check.credit.currency] = param[item.check.credit.currency] || 0;

            param[item.check.debit.currency] += item.check.debit.balance;
            param[item.check.credit.currency] += item.check.credit.balance;
        });
        
        return this.serviceRequest(param, (param: Record<string, number> , currency: Record<string, number>) => {
            let result = param.uah || 0;
            for(let i in currency) {
                for(let j in param){
                    if(i === j){
                        result += param[j] * currency[i];
                    } 
                }
            }

            return result;
        });
    }

    oweBank() {
        let param: Record<string, number> = {};

        this.bankClients.forEach(item => {
            if (item.check.credit.creditLimit >= item.check.credit.balance) {
                param[item.check.credit.currency] = param[item.check.credit.currency] || 0;
                param[item.check.credit.currency] += item.check.credit.creditLimit - item.check.credit.balance;
            }
        });

        return this.serviceRequest(param, (param: Record<string, number>, currency: Record<string, number>) => {
            let result = param.uah || 0;
            for(let i in currency) {
                for(let j in param){
                    if(i === j){
                        result += param[j] * currency[i];
                    } 
                }
            }

            return result;
        });
    }

    customersOweBank(argument: boolean) {
        let param: Record<string, number> = {};

        this.bankClients.forEach(item => {
            if (item.check.credit.creditLimit >= item.check.credit.balance && item.activeClients === argument) {
                param[item.check.credit.currency] = param[item.check.credit.currency] || 0;
                param[item.check.credit.currency] += item.check.credit.creditLimit - item.check.credit.balance;
            }
        });

        return this.serviceRequest(param, (param: Record<string, number>, currency: Record<string, number>) => {
            let result: number = param.uah || 0;
            for(let i in currency) {
                for(let j in param){
                    if(i === j){
                        result += param[j] * currency[i];
                    } 
                }
            }

            return result;
        });
    }
}

const bank = new Bank(bankClients);

class RenderCardsBank{
    container: HTMLElement | null;
    modal: HTMLElement | null;
    trueAdnFalse: boolean | null;
    eventActive: Record<string, boolean>;

    constructor(){
        this.container = document.querySelector<HTMLElement>('.container') as HTMLElement;
        this.modal = document.querySelector<HTMLElement>('.modal') as HTMLElement;
        this.trueAdnFalse = null;
        this.eventActive = {
            "true": true,
            "false": false
        }
        this.renderUsers();
    }

    createCard(item: Clients, index: number){
        const cart: HTMLDivElement = document.createElement('div');
        cart.className = 'usersContainer';
        cart.innerHTML = `
            <p>name: ${item.name}</p>
            <p>surname: ${item.surname}</p>
            <p>active clients: ${item.activeClients}</p>
            <p>registration date: ${item.registrationDate}</p>
            <div class="userCheck">
                <div class="cartDebit">
                    <h3>debit</h3>
                    <p>balance: ${item.check.debit.balance}</p>
                    <p>date issue: ${item.check.debit.dateIssue}</p>
                    <p>end term: ${item.check.debit.endTerm}</p>
                    <p>currency: ${item.check.debit.currency}</p>
                    <p>active: ${item.check.debit.active}</p>
                </div>
                <div class="cartCredit">
                    <h3>credit</h3>
                    <p>balance: ${item.check.credit.balance}</p>
                    <p>credit limit: ${item.check.credit.creditLimit}</p>
                    <p>date issue: ${item.check.credit.dateIssue}</p>
                    <p>end term: ${item.check.credit.endTerm}</p>
                    <p>currency: ${item.check.credit.currency}</p>
                    <p>active: ${item.check.credit.active}</p>
                </div>
            </div>
        `;

        const buttonChange: HTMLButtonElement = document.createElement('button');
        const buttonDelete: HTMLButtonElement = document.createElement('button');
        
        buttonChange.innerText = 'chenge';
        buttonDelete.innerText = 'delete';
        
        buttonDelete.addEventListener('click', this.deleteUsers.bind(this, index));
        buttonChange.addEventListener('click', this.changeUser.bind(this, item, index));

        cart.append(buttonChange, buttonDelete);
       
        return cart;
    }

    renderUsers(){
        this.container!.innerHTML = '';
        const cards: HTMLElement[] = bankClients.map((item, index) => this.createCard(item, index));
        this.container!.append(...cards);
    }

    createModalUser(item?: Clients, index?: number){
        this.modal!.innerHTML = '';

        const modalChange: HTMLFormElement = document.createElement('form');
        modalChange.className = 'modalChange';
        modalChange.innerHTML = `
            <form>
                <span>name: <input name='name' placeholder = '${this.trueAdnFalse ? item!.name : ''}' type="text"></span>
                <span>surname: <input name='surname' placeholder = '${this.trueAdnFalse ? item!.surname : ''}' type="text"></span>
                <span>active clients:
                <select name='activeClients' class="eventSelect">
                    <option value='${this.trueAdnFalse ? item!.activeClients : ''}'>${this.trueAdnFalse ? item!.activeClients : ''}</option>
                </select></span>
                <span>registration date: <input name='registrationDate' placeholder = '${this.trueAdnFalse ? item!.registrationDate : ''}' type="date"></span>
                <div class="userCheck">
                    <div class="cartDebit">
                        <h3>debit</h3>
                        <span>balance: <input name='balanceDebit' placeholder = '${this.trueAdnFalse ? item!.check.debit.balance : ''}' type="number"></span>
                        <span>date issue: <input name='dateIssueDebit' placeholder = '${this.trueAdnFalse ? item!.check.debit.dateIssue : ''}' type="date"></span>
                        <span>end term: <input name='endTermDebit' placeholder = '${this.trueAdnFalse ? item!.check.debit.endTerm : ''}' type="date"></span>
                        <span>currency: 
                        <select name='currencyDebit' class="select">
                            <option value='${this.trueAdnFalse ? item!.check.debit.currency : ''}'>${this.trueAdnFalse ? item!.check.debit.currency : ''}</option>
                        </select></span>
                        <span>active: 
                        <select name='activeDebit' class="eventSelect">
                            <option value='${this.trueAdnFalse ? item!.check.debit.active : ''}'>${this.trueAdnFalse ? item!.check.debit.active : ''}</option>
                        </select></span>
                    </div>
                    <div class="cartCredit">
                        <h3>credit</h3>
                        <span>balance: <input name='balanceCredit' placeholder = '${this.trueAdnFalse ? item!.check.credit.balance : ''}' type="number"></span>
                        <span>credit limit: <input name='creditLimit' placeholder = '${this.trueAdnFalse ? item!.check.credit.creditLimit : ''}' type="number"></span>
                        <span>date issue: <input name='nadateIssuemeCredit' placeholder = '${this.trueAdnFalse ? item!.check.credit.dateIssue : ''}' type="date"></span>
                        <span>end term: <input name='endTermCredit' placeholder = '${this.trueAdnFalse ? item!.check.credit.endTerm : ''}' type="date"></span>
                        <span>currency: 
                        <select name='currencyCredit' class="select">
                            <option value='${this.trueAdnFalse ? item!.check.credit.currency : ''}'>${this.trueAdnFalse ? item!.check.credit.currency : ''}</option>
                        </select></span>
                        <span>active: 
                        <select name='activeCredit' class="eventSelect">
                            <option value='${this.trueAdnFalse ? item!.check.credit.active : ''}'>${this.trueAdnFalse ? item!.check.credit.active : ''}</option>
                        </select>
                        </span>
                    </div>
                </div>
            </form>
        `;
       
        const buttonAdd: HTMLButtonElement = document.createElement('button');
        buttonAdd.innerText = 'Add';

        buttonAdd.addEventListener('click', this.chengeUserAndAddUser.bind(this, index!));
        if(this.modal !== null){
            this.modal.addEventListener('click', this.closeModal.bind(this, this.modal));
        }

        modalChange.append(buttonAdd);
        this.modal!.append(modalChange);
        
        const eventSelect: NodeListOf<HTMLElement> = document.querySelectorAll('.eventSelect');
        eventSelect.forEach(function(userItem) {
            userItem.innerHTML += `
                <option value='false'>false</option>
                <option value='true'>true</option>
            `
        });
          
        const select: NodeListOf<HTMLElement> = document.querySelectorAll('.select');
        select.forEach(function(userItem) {
            userItem.innerHTML += `
                <option value='RUB'>RUB</option>
                <option value='USD'>USD</option>
                <option value='EUR'>EUR</option>
                <option value='UAH'>UAH</option>
            `
        });
    }
    
    onChengeUser(event: any, index: number){
        event.preventDefault();
        
        let data: FormData = new FormData(event.target.closest('form'));

        bankClients[index].name = String(data.get('name')) || bankClients[index].name;
        bankClients[index].surname = String(data.get('surname')) || bankClients[index].surname;
        bankClients[index].activeClients = this.eventActive[String(data.get('activeClients'))];
        bankClients[index].registrationDate = String(data.get('registrationDate')) || bankClients[index].registrationDate;

        bankClients[index].check.debit.balance = Number(data.get('balanceDebit')) || bankClients[index].check.debit.balance;
        bankClients[index].check.debit.dateIssue = String(data.get('dateIssueDebit')) || bankClients[index].check.debit.dateIssue;
        bankClients[index].check.debit.endTerm = String(data.get('endTermDebit')) || bankClients[index].check.debit.endTerm;
        bankClients[index].check.debit.currency = String(data.get('currencyDebit')) || bankClients[index].check.debit.currency;
        bankClients[index].check.debit.active = this.eventActive[String(data.get('activeDebit'))];

        bankClients[index].check.credit.balance = Number(data.get('balanceCredit')) || bankClients[index].check.credit.balance;
        bankClients[index].check.credit.creditLimit = Number(data.get('creditLimit')) || bankClients[index].check.credit.creditLimit;
        bankClients[index].check.credit.dateIssue = String(data.get('dateIssueCredit')) || bankClients[index].check.credit.dateIssue;
        bankClients[index].check.credit.endTerm = String(data.get('endTermCredit')) || bankClients[index].check.credit.endTerm;
        bankClients[index].check.credit.currency = String(data.get('currencyCredit')) || bankClients[index].check.credit.currency;
        bankClients[index].check.credit.active = this.eventActive[String(data.get('activeCredit'))];

        this.modal!.classList.remove('active');
        this.renderUsers();
    }

    addUser(event: any){
        event.preventDefault();

        let data: FormData = new FormData(event.target.closest('form'));

        bankClients.push({
            name: String(data.get('name')) || '',
            surname: String(data.get('surname')) || '',
            activeClients: this.eventActive[String(data.get('activeClients'))],
            registrationDate: String(data.get('registrationDate')) || '',
            check: {
                debit: {
                    balance: Number(data.get('balanceDebit')) || 0,
                    dateIssue: String(data.get('dateIssueDebit')) || '',
                    endTerm: String(data.get('endTermDebit')) || '',
                    currency: String(data.get('currencyDebit')) || '',
                    active: this.eventActive[String(data.get('activeDebit'))],
                },
                credit: {
                    balance: Number(data.get('balanceCredit')) || 0,
                    creditLimit: Number(data.get('creditLimit')) || 0,
                    dateIssue: String(data.get('dateIssueCredit')) || '',
                    endTerm: String(data.get('endTermCredit')) || '',
                    currency: String(data.get('currencyCredit')) || '',
                    active: this.eventActive[String(data.get('activeCredit'))],
                },
            }
        });

        this.renderUsers();
        this.modal!.classList.remove('active');
    }

    closeModal(param: HTMLElement, event: MouseEvent){
        if (param === event.target) {
            param.classList.remove('active');
        }
    }

    chengeUserAndAddUser(index: number, event: MouseEvent){
        if(this.trueAdnFalse){
            this.onChengeUser(event, index);
        } else if(this.trueAdnFalse === false){
            this.addUser(event);
        }
    }

    changeUser(item: Clients, index: number){
        this.modal!.classList.add('active');
        this.trueAdnFalse = true;
        this.createModalUser(item, index);
    }

    deleteUsers (index: number){
        bankClients.splice(index, 1);
        this.renderUsers();
    }
}

const renderCardsBank = new RenderCardsBank();

class StatisticsBank{
    statisticsModal: HTMLElement | null;
    totalAmountMoneytBlock: HTMLElement | null;
    oweBankBlock: HTMLElement | null;
    customersOweBankResultBlock: HTMLElement | null;
    customersOweBankButtons: HTMLElement | null;

    constructor(){
        this.statisticsModal = document.querySelector<HTMLElement>('.statisticsModal') as HTMLElement;
        this.totalAmountMoneytBlock = document.querySelector<HTMLElement>('.totalAmountMoneyResult') as HTMLElement;
        this.oweBankBlock = document.querySelector<HTMLElement>('.oweBankResult') as HTMLElement;
        this.customersOweBankResultBlock = document.querySelector<HTMLElement>('.customersOweBankResult') as HTMLElement;
        this.customersOweBankButtons = document.querySelector<HTMLElement>('.customersOweBankButtons') as HTMLElement;
    }

    statistics(){
        const totalAmountMoneyResult: Promise<number> = bank.totalAmountMoney();
        const oweBankResult: Promise<number> = bank.oweBank();
        const customersOweBankFalseResult: Promise<number> = bank.customersOweBank(false);
        const customersOweBankTrueResult: Promise<number> = bank.customersOweBank(true);
        this.customersOweBankButtons!.innerHTML = '';
        
        totalAmountMoneyResult.then(data => {
            this.totalAmountMoneytBlock!.innerText = `${data}`;
        });
        oweBankResult.then(data => {
            this.oweBankBlock!.innerText = `${data}`;
        });
        customersOweBankFalseResult.then(data => {
            this.customersOweBankResultBlock!.innerText = `${data}`;
        });
        
        const activeBatton: HTMLButtonElement = document.createElement('button');
        const isActiveBatton: HTMLButtonElement = document.createElement('button');

        activeBatton.innerText = 'Active';
        isActiveBatton.innerText = 'No active';    

        activeBatton.addEventListener('click', this.statisticsActive.bind(this, customersOweBankFalseResult));
        isActiveBatton.addEventListener('click', this.statisticsActive.bind(this, customersOweBankTrueResult));
        if(this.statisticsModal !== null){
            this.statisticsModal.addEventListener('click', this.closeStatistics.bind(this, this.statisticsModal));
        }

        this.customersOweBankButtons!.append(activeBatton, isActiveBatton);
    }

    closeStatistics(param: HTMLElement, event: MouseEvent){
        if (param === event.target) {
            param.classList.remove('active');
        }
    }

    statisticsActive(param: any){
        this.customersOweBankResultBlock!.innerText = '';
        param.then((data: number) => {
            this.customersOweBankResultBlock!.innerText = `${data}`;
        })
    }
}

const statisticsBank = new StatisticsBank();

class HeaderBank extends RenderCardsBank{
    statisticsBank: StatisticsBank;
    statisticsModal: HTMLElement | null;
    header: HTMLElement | null;

    constructor(){
        super();
        this.statisticsBank = statisticsBank;
        this.statisticsModal = document.querySelector<HTMLElement>('.statisticsModal') as HTMLElement;
        this.header = document.querySelector<HTMLElement>('.header') as HTMLElement;
        this.headerButtons();
    }
    
    headerButtons(){
        const statistics: HTMLButtonElement = document.createElement('button');
        const addUserButton: HTMLButtonElement = document.createElement('button');
        const restaurantLink: HTMLAnchorElement = document.createElement('a');
        const bankLink: HTMLAnchorElement = document.createElement('a');
        
        statistics.innerText = 'Statistics';
        addUserButton.innerText = 'Add user';
        restaurantLink.innerText = 'Restaurant';
        restaurantLink.href = './restauran.html';
        bankLink.innerText = 'Bank';
        bankLink.href = './bankHtml.html';

        addUserButton.addEventListener('click', this.addUserModal.bind(this));
        statistics.addEventListener('click', this.onStatistics.bind(this));
        
        return this.header!.append(statistics, addUserButton, restaurantLink, bankLink);
    }
    
    onStatistics(){
        this.statisticsBank.statistics();
        this.statisticsModal!.classList.add('active');
    }

    addUserModal(){
        this.modal!.classList.add('active');
        this.trueAdnFalse = false;
        this.createModalUser();
    }
}

const headerBank = new HeaderBank();
